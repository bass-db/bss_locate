This repository contains version 2 of BSS Locate dated from March 21, 2012, and previously available at http://bass-db.gforge.inria.fr/bss_locate/.


# Purpose
BSS Locate is a Matlab toolbox that estimates the **time differences of arrival (TDOAs)** of **multiple sources** in a **2-channel audio signal** recorded by a pair of **omnidirectional microphones**. The number of sources and the distance between the two microphones are assumed to be known but are not restricted to a given range.

BSS Locate implements 12 different source localization methods:
- 8 methods based on the detection of the peaks of an angular spectrum (GCC-PHAT, GCC-NONLIN, MUSIC and several SNR-based spectra)
- 4 methods based on clustering of the time-frequency bins

GCC-NONLIN provides the best results over a wide range of configurations.

**To estimate the directions-of-arrival (DOAs) of multiple sources recorded with an array of 3 or more microphones, use [Multichannel BSS Locate](https://gitlab.inria.fr/bass-db/mbss_locate) instead.**

# Usage
Two categories of source localization methods are implemented:
- angular spectrum-based methods (bss_locate_spec.m)
- clustering-based methods (bss_locate_clus.m)

In order to apply the most accurate method to date (GCC-NONLIN), just type

    tau = bss_locate_spec(x, fs, d, nsrc)

where x is an nsampl x 2 matrix containing a stereo signal, fs is the sampling frequency in Hz, d is the microphone spacing in meters, and nsrc is the number of sources. The output tau is then the 1 x nsrc vector of estimated TDOAs in seconds.

For information about the syntax, type

    help bss_locate_spec
    help bss_locate_clus


# Reproducibility
The toolbox comes with the source signals and the mixing filters used for evaluation in [[1]](#1).

The filters are named <J>src_<RT>_<d>_<r>_config<k>.mat where <J> is the
number of sources, <RT> the reverberation time, <d> the microphone spacing,
<r> the distance between the sources and the center of the microphone pair and
<k> the index of one configuration satisfying the above characteristics.

To generate the signals used for evaluation, add the directories sources/ and filters/ to the Matlab path, and type

    x = mix(fname, stype);

where fname is one of the mixing filter filenames in the directory filters/
and stype is 'female', 'male' or 'music'

To evaluate the results of your algorithm, type

    taue = true_tdoas(fname);
    [R, P, F, Acc] = eval_tdoas(taue, tau, d, 5);


# Reference
<a id="1">[1]</a> C. Blandin, A. Ozerov, and E. Vincent, ["Multi-source TDOA estimation in reverberant audio using angular spectra and clustering"](https://hal.inria.fr/inria-00630994v2/document), *Signal Processing* 92, pp. 1950-1960, 2012.


# License
The Matlab M-files bss_locate_clus.m, bss_locate_spec.m, qstft.m  and stft_multi.m are authored by Charles Blandin and/or Emmanuel Vincent and distributed under the terms of the [GNU Public License version 3](http://www.gnu.org/licenses/gpl.txt).

The Matlab MAT-files in the directory "filters" are authored by Charles Blandin and distributed under the the terms of the [Creative Commons Attribution-NonCommercial-ShareAlike 2.0 license](http://creativecommons.org/licenses/by-nc-sa/2.0/).

The WAV audio files music_s1.wav, music_s2.wav, music_s3.wav, music_s4.wav and music_s5.wav are authored by [Shannon Hurley](http://amiestreet.com/artist/shannon-hurley/) and distributed under the the terms of the [Creative Commons Attribution-NonCommercial 3.0 license](http://creativecommons.org/licenses/by-nc/3.0/).

The WAV audio file music_s6.wav is authored by [The Ultimate NZ Tour](http://newzealandeducated.com/contest/ultimate-nz-tour-i/) and distributed under the the terms of the [Creative Commons Attribution-Noncommercial-ShareAlike 3.0 license](http://creativecommons.org/licenses/by-nc-sa/3.0/).

All other WAV audio files are distributed under the the terms of the [Creative Commons Attribution-NonCommercial-ShareAlike 2.0 license](http://creativecommons.org/licenses/by-nc-sa/2.0/).